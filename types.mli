(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   types.mli                                          :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2018/10/24 09:14:51 by pmatle            #+#    #+#             *)
(*   Updated: 2018/10/24 09:14:53 by pmatle           ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

type starts = string * string
type finals = string * string * string
type condition = {
    start : starts;
    final : finals
}
