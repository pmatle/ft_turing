(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   turing.ml                                          :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2018/10/24 08:34:45 by pmatle            #+#    #+#             *)
(*   Updated: 2018/11/08 11:03:38 by pmatle           ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

class turing descriptions start input =
    object(self)
        val _descriptions : Machine.machine = descriptions
        val _state : string = start
        val _input : string = input
        val _index : int = 0
        val _error : bool = false
        val _error_message : string = ""

        method is_error = _error
        method get_error_message = _error_message

        method private check_state lst =
            begin
                let (state, value) = lst.Types.start in
                let character = String.get _input _index in
                if (String.equal state _state) && (String.equal value (String.make 1 character))
                then true
                else false
            end

        method private print_name =
            begin
                let name = _descriptions#retrieve_name in
                print_endline "*****************************************************";
                print_endline "*";
                print_string "*                   ";
                print_endline name;
                print_endline "*";
                print_endline "*****************************************************"
            end

        method private print_state =
            begin
                let rec print = function
                    | [] -> print_endline "******************************************************"
                    | elem :: remain ->
                            begin
                                let (curr_state, curr_val) = elem.Types.start in
                                print_string ("(" ^ curr_state ^ ", " ^ curr_val ^ ")");
                                let (state, value, action) = elem.Types.final in
                                print_string " -> ";
                                print_endline ("(" ^ state ^ ", " ^ value ^ ", " ^ action ^ ")");
                                print remain
                            end
                in
                print _descriptions#retrieve_transition
            end

        method print_info =
            begin
                let alphabets = String.concat ", " _descriptions#retrieve_alphabet in
                let finals = String.concat ", " _descriptions#retrieve_finals in
                let states = String.concat ", " _descriptions#retrieve_states in
                self#print_name;
                print_endline ("Alphabet: [ " ^ alphabets ^ " ]");
                print_endline ("States  : [ " ^ states ^ " ]");
                print_endline ("Initial : " ^ _descriptions#retrieve_initial);
                print_endline ("Finals  : [ " ^ finals ^ " ]");
                self#print_state
            end

        method get_state = _state
        method get_tape = _input

        method private iter_steps record =
            begin
                let (state_i, value_i) = record.Types.start in
                let (state_f, value_f, action) = record.Types.final in
                let read_state = "(" ^ state_i ^ ", " ^ value_i ^ ")" in
                let next = " -> " in
                let write_state = "(" ^ state_f ^ ", " ^ value_f ^ ", " ^ action ^ ")" in
                read_state ^ next ^ write_state
            end

        method private steps str index =
                let len = String.length str in
                let cur_index = ("<" ^ String.make 1 str.[index] ^ ">") in
                let second_part = String.sub str (index + 1) (len - (index + 1)) in
                if index = 0 then
                    "[" ^ cur_index ^ second_part ^ "]"
                else if index = (len - 1) then
                    let first_part = String.sub str 0 (len - 1) in
                    "[" ^ first_part ^ cur_index ^ "]"
                else
                    let first_part = String.sub str 0 index in
                    "[" ^ first_part ^ cur_index ^ second_part ^ "]"

        method run_machine =
            begin
                try
                    if (_index < 0) || (_index >= String.length _input)
                    then {<_error = true; _error_message = "Error: Index out of bound">}
                    else
                        begin
                            let record = List.find self#check_state _descriptions#retrieve_transition in
                            let (state, value, action) = record.final in
                            let tmp = Bytes.of_string _input in
                            let output =  Bytes.to_string tmp in
                            print_endline ((self#steps output _index) ^ " " ^ (self#iter_steps record));
                            Bytes.set tmp _index (String.get value 0);
                            if String.equal action "LEFT"
                            then
                                begin
                                    if List.mem state _descriptions#retrieve_finals
                                    then print_endline ((self#steps (Bytes.to_string tmp) (_index - 1)));
                                    {<_state = state; _index = (_index - 1); _input = (Bytes.to_string tmp)>}
                                end
                            else
                                begin
                                    if List.mem state _descriptions#retrieve_finals
                                    then print_endline ((self#steps (Bytes.to_string tmp) (_index + 1)));
                                    {<_state = state; _index = (_index + 1); _input = (Bytes.to_string tmp)>}
                                end
                        end
                with
                | Not_found ->
                        begin
                            let error = "Error: The input given is not in the right format" in
                            {<_error = true; _error_message = error>}
                        end
            end
    end
