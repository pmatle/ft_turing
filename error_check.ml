(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   error_check.ml                                     :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: ckatz <marvin@42.fr>                       +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2018/10/19 11:20:39 by ckatz             #+#    #+#             *)
(*   Updated: 2018/11/07 17:24:30 by ckatz            ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let in_list str lst =
    List.mem str lst

let check_lists list_one list_two =
    let len = List.length list_one in
    let rec loop list_one list_two index valid =
        if (index = len) then
            valid
        else
            begin
                let element = List.nth list_one index in
                let valid = in_list element list_two in
                loop list_one list_two (index + 1) valid
            end
    in
    loop list_one list_two 0 false

let loop_transitions transitions trans_lst alpha_lst =
    let len = List.length transitions in
    let rec loop index valid =
    if (index = len) then
        valid
    else
        begin
            let current = List.nth transitions index in
            let (transition, read) = current.Types.start in
            let (to_state, write, action) = current.Types.final in
            let check_1 = ((in_list transition trans_lst) && (in_list read alpha_lst)) in
            let check_2 = ((in_list to_state trans_lst) && (in_list write alpha_lst)) in
            let check_3 = ((action = "LEFT") || (action = "RIGHT")) in
            if ((check_1) && (check_2) && (check_3)) then
                loop (index + 1) true
            else
                false
        end
    in
    loop 0 false
           
let validate_input machine =
    let alphabet = machine#retrieve_alphabet in
    let blank = machine#retrieve_blank in
    let states = machine#retrieve_states in
    let initial = machine#retrieve_initial in
    let finals = machine#retrieve_finals in
    let transitions = machine#retrieve_transition in
    let check_1 = in_list blank alphabet in
    let check_2 = in_list initial states in
    let check_3 = check_lists finals states in
    let check_4 = loop_transitions transitions states alphabet in
    if ((check_1) && (check_2) && (check_3) && (check_4)) then
        true
    else
        false
