(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   parser.ml                                          :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2018/10/18 14:43:33 by pmatle            #+#    #+#             *)
(*   Updated: 2018/11/07 16:39:44 by pmatle           ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

class machine json =
    let open Yojson.Basic.Util in
    object (this)
        val _json : Yojson.Basic.json = json
        val _name : string = ""
        val _alphabet : string list = []
        val _blank : string = ""
        val _states : string list = []
        val _initial : string = ""
        val _finals : string list = []
        val _transitions : Types.condition list = []
        val _error : bool = false
        val _error_message : string = ""

        method error_happened = _error
        method retrieve_error = _error_message
        method retrieve_name = _name
        method retrieve_alphabet = _alphabet
        method retrieve_blank = _blank
        method retrieve_states = _states
        method retrieve_initial = _initial
        method retrieve_finals = _finals
        method retrieve_transition = _transitions

        method store_name =
            begin
                try
                    let name = _json |> member "name" |> to_string in
                    {<_name = name>}
                with
                | Type_error (err, json) -> {< _error = true; _error_message = err >}
                | Undefined (err, json) -> {< _error = true; _error_message = err >}
            end

        method store_alphabet =
            begin
                try
                    let alphabet = _json |> member "alphabet" |> to_list |> filter_string in
                    {<_alphabet = alphabet>}
                with
                | Type_error (err, json) -> {< _error = true; _error_message = err >}
                | Undefined (err, json) -> {< _error = true; _error_message = err >}
            end

        method store_blank  =
            begin
                try
                    let blank = _json |> member "blank" |> to_string in
                    {<_blank = blank>}
                with
                | Type_error (err, json) -> {< _error = true; _error_message = err >}
                | Undefined (err, json) -> {< _error = true; _error_message = err >}
            end

        method store_states =
            begin
                try
                    let states = _json |> member "states" |> to_list |> filter_string in
                    {<_states = states>}
                with
                | Type_error (err, json) -> {< _error = true; _error_message = err >}
                | Undefined (err, json) -> {< _error = true; _error_message = err >}
            end

        method store_initial =
            begin
                try
                    let initial = _json |> member "initial" |> to_string in
                    {<_initial = initial>}
                with
                | Type_error (err, json) -> {< _error = true; _error_message = err >}
                | Undefined (err, json) -> {< _error = true; _error_message = err >}
            end

        method store_finals =
            begin
                try
                    let finals = _json |> member "finals" |> to_list |> filter_string in
                    {<_finals = finals>}
                with
                | Type_error (err, json) -> {< _error = true; _error_message = err >}
                | Undefined (err, json) -> {< _error = true; _error_message = err >}
            end

        method private store_values json elem =
            begin
                try
                let index = json |> member elem |> to_list in
                let read = List.map (fun value -> member "read" value |> to_string) index in
                let to_state = List.map (fun value -> member "to_state" value |> to_string) index in
                let write = List.map (fun value -> member "write" value |> to_string) index in
                let action = List.map (fun value -> member "action" value |> to_string) index in
                let rec make_records count new_list =
                    if count = (List.length read)
                    then new_list
                    else
                        begin
                            let read_val = List.nth read count in
                            let state_val = List.nth to_state count in
                            let write_val = List.nth write count in
                            let action_val = List.nth action count in
                            let first = (elem, read_val) in
                            let second = (state_val, write_val, action_val) in
                            let record = {Types.start = first; Types.final = second} in
                            make_records (count + 1) (record :: new_list)
                        end
                in
                make_records 0 []
                with
                | Type_error (err, json) -> raise Exit
                | Undefined (err, json) -> raise Exit
            end

        method store_transitions =
            begin
                let rec make_list group json acc count = match group with
                    | [] -> acc
                    | elem :: remain ->
                            begin
                                if (List.exists (fun value -> String.equal elem value) _finals) = true
                                then make_list remain json acc (count + 1)
                                else
                                    begin
                                        try
                                            let state_list = this#store_values json elem in
                                            make_list remain json (List.append acc state_list) (count + 1)
                                        with
                                        | Exit -> []
                                    end
                            end
                in
                let trans = _json |> member "transitions" in
                let transitions = make_list _states trans [] 0 in
                if (List.length transitions == 0)
                then {<_error = true; _error_message = "Error: Transitions not formatted properly">}
                else {<_transitions = transitions>}
            end
    end
