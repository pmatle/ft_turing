(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   main.ml                                            :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: pmatle <marvin@42.fr>                      +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2018/10/18 16:41:45 by pmatle            #+#    #+#             *)
(*   Updated: 2018/11/08 11:08:49 by pmatle           ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let read_file path =
    let json = Yojson.Basic.from_file path in
    let test = new Machine.machine json in
    let test = test#store_name in
    let test = test#store_alphabet in
    let test = test#store_blank in
    let test = test#store_states in
    let test = test#store_initial in
    let test = test#store_finals in
    let test = test#store_transitions in
    test

let check_error file =
    try
        if Sys.file_exists file
        then
            begin
                ignore(Yojson.Basic.from_file file);
                (string_of_bool false)
            end
        else (string_of_bool true)
    with
    | Yojson.Json_error err -> err

let print_help () =
    print_endline "usage: ft_turing [-h] jsonfile input\n";
    print_endline "positional arguments:";
    print_endline "\tjsonfile\t\tjson description of the machine\n";
    print_endline "\tinput\t\t\tinput of the machine\n";
    print_endline "optional arguments:";
    print_endline "\t-h, --help show this help message and exit"

let arg_check str alphabet_lst =
    let len = String.length str in
    let rec loop index =
        if (index == len)
        then
            true
        else
            begin
                let current_char = String.get str index in
                let char_string = String.make 1 current_char in
                if (List.mem char_string alphabet_lst)
                then
                    loop (index + 1)
                else
                    false
            end
    in
    loop 0

let execute_turing file arg =
    begin
        let input = read_file file in
        let input_check = arg_check arg input#retrieve_alphabet in
        let json_check = Error_check.validate_input input in
        let file_check = input#error_happened in
        if (input_check && json_check && (file_check = false))
        then
            let data = arg ^ (String.make 50 '.')  in
            let start = input#retrieve_initial in
            let turing = new Turing.turing input start data in
            turing#print_info;
            let rec execute machine =
                begin
                    if not (List.mem machine#get_state input#retrieve_finals)
                    then
                        begin
                            let machine = machine#run_machine in
                            if machine#is_error
                            then print_endline ("\n" ^ (machine#get_error_message))
                            else ignore(execute machine)
                        end
                end
            in
            execute turing
        else
            print_endline "Input error detected"

    end

let check_params argc argv =
    if argc = 2
    then
        begin
            let help = argv.(1) in
            if ((help = "-h") || (help = "--help"))
            then print_help ()
            else print_endline "Invalid input, use -h or --help to check input parameters"
        end
            else
                print_endline "Invalid input, use -h or --help to check input parameters"

let main argc argv =
    if argc = 3
    then
        begin
            let message = check_error argv.(1) in
            if String.equal message "true"
            then print_endline "Invalid machine discription detected"
            else if not (String.equal message "false")
            then print_endline message
            else ignore(execute_turing argv.(1) argv.(2))
        end
            else
                check_params argc argv

let () =
    main (Array.length Sys.argv) Sys.argv
