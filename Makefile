# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pmatle <marvin@42.fr>                      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/10/11 15:51:30 by pmatle            #+#    #+#              #
#*   Updated: 2018/10/23 22:49:20 by mafernan         ###   ########.fr       *#
#                                                                              #
# **************************************************************************** #

RESULT = ft_turing
SOURCES = types.mli machine.ml error_check.ml turing.ml main.ml
PACKS = yojson
OCAMLMAKEFILE = OCamlMakefile
include $(OCAMLMAKEFILE)
