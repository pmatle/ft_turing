# FT_TURING

The goal of this project is to write a program able to simulate a single headed, single tape Turing machine from a machine description provided in json.
I used this project to write OCaml code in the context of a real program.

## The Turing Machine

You must write a program able to simulate a single headed and single tape Turing machine from a json machine description given as a parameter to your program.  
The json machine description is slightly simplier than a formal description of the same machine.  
This is a valid exemple of a json machine description for this project:
```bash
$> cat unary_sub.json
{
    "name" : "unary_sub",
    "alphabet": [ "1", ".", "-", "=" ],
    "blank" : ".",
    "states" : [ "scanright", "eraseone", "subone", "skip", "HALT" ],
    "initial" : "scanright",
    "finals" : [ "HALT" ],
    "transitions" : {
        "scanright": [
            { "read" : ".", "to_state": "scanright", "write": ".", "action": "RIGHT"},
            { "read" : "1", "to_state": "scanright", "write": "1", "action": "RIGHT"},
            { "read" : "-", "to_state": "scanright", "write": "-", "action": "RIGHT"},
            { "read" : "=", "to_state": "eraseone" , "write": ".", "action": "LEFT" }
         ],
        "eraseone": [
            { "read" : "1", "to_state": "subone", "write": "=", "action": "LEFT"},
            { "read" : "-", "to_state": "HALT" , "write": ".", "action": "LEFT"}
        ],
        "subone": [
            { "read" : "1", "to_state": "subone", "write": "1", "action": "LEFT"},
            { "read" : "-", "to_state": "skip" , "write": "-", "action": "LEFT"}
        ],
        "skip": [
            { "read" : ".", "to_state": "skip" , "write": ".", "action": "LEFT"},
            { "read" : "1", "to_state": "scanright", "write": ".", "action": "RIGHT"}
        ]
    }
}
$>
```

The json fields are defined as follows:  

* name: The name of the described machine  
* alphabet: Both input and work alphabet of the machine merged into a single alphabet for simplicity’s sake, including the blank character. Each character of the alphabet must be a string of length strictly equal to 1.  
* blank: The blank character, must be part of the alphabet, must NOT be part of the input.  
* states: The exhaustive list of the machine’s states names.  
* initial: The initial state of the machine, must be part of the states list.  
* finals: The exhaustive list of the machine’s final states. This list must be a sub-list of the states list.  
* transitions: A dictionnary of the machine’s transitions indexed by state name. Each transition is a list of dictionnaries, and each dictionnary describes the transition for a given character under the head of the machine. A transition is defined as follows:  
    * read: The character of the machine’s alphabet on the tape under the machine’s head.  
    * to_state: The new state of the machine after the transition is done.  
    * write: The character of the machine’s alphabet to write on the tape before moving the head.  
    * action: Movement of the head for this transition, either LEFT, or RIGHT.  
		
The program must detect and reject ill formated or invalid machine descriptions and inputs, with a relevant error message. This means that the program must never crash for any reason.  

## Machine Descriptions

Five machine descriptions are given the project folder:  

1. A machine able to compute an unary addition.  
    * Filename: unary_add.json
2. A machine able to decide whether its input is a palindrome or not. Before halting, write the result on the tape as a ’n’ or a ’y’ at the right of the rightmost character of the tape.  
    * Filename: palindome.json
3. A machine able to decide if the input is a word of the language 0n1n, for instance the words 000111 or 0000011111. Before halting, write the result on the tape as a ’n’ or a ’y’ at the right of the rightmost character of the tape.  
    * Filename: 0n_1n.json
4. A machine able to decide if the input is a word of the language 02n, for instance the words 00 or 0000, but not the words 000 or 00000. Before halting, write the result on the tape as a ’n’ or a ’y’ at the right of the rightmost character of the tape.  
    * Filename: 02n_language.json
5. A machine able to run the first machine of this list, the one computing an unary addition. The machine alphabet, states, transitions and input ARE the input of the machine you are writing, encoded as you see fit.
    * Filename: UTM.json

## Compilation

Use the Makefile provided to compile the executable for the program.

```bash
~$ make
```

## Usage

The program has the following usage:
```bash
$> ./ft_turing --help
usage: ft_turing [-h] jsonfile input

positional arguments:  
    jsonfile json description of the machine

    input input of the machine

optional arguments:  

    -h, --help show this help message and exit
```

The program must output at least the state of the tape with a visible representation of the head at each transition. For instance :
```bash
$> ./ft_turing res/unary_sub.json "111-11="
********************************************************************************
*                                                                              *
*                                 unary_sub                                    *
*                                                                              *
********************************************************************************
Alphabet: [ 1, ., -, = ]
States : [ scanright, eraseone, subone, skip, HALT ]
Initial : scanright
Finals : [ HALT ]
(scanright, .) -> (scanright, ., RIGHT)
(scanright, 1) -> (scanright, 1, RIGHT)
(scanright, -) -> (scanright, -, RIGHT)
(scanright, =) -> (eraseone, ., LEFT)
(eraseone, 1) -> (subone, =, LEFT)
(eraseone, -) -> (HALT, ., LEFT)
(subone, 1) -> (subone, 1, LEFT)
(subone, -) -> (skip, -, LEFT)
(skip, .) -> (skip, ., LEFT)
(skip, 1) -> (scanright, ., RIGHT)
********************************************************************************
[<1>11-11=.............] (scanright, 1) -> (scanright, 1, RIGHT)
[1<1>1-11=.............] (scanright, 1) -> (scanright, 1, RIGHT)
[11<1>-11=.............] (scanright, 1) -> (scanright, 1, RIGHT)
[111<->11=.............] (scanright, -) -> (scanright, -, RIGHT)
[111-<1>1=.............] (scanright, 1) -> (scanright, 1, RIGHT)
[111-1<1>=.............] (scanright, 1) -> (scanright, 1, RIGHT)
[111-11<=>.............] (scanright, =) -> (eraseone, ., LEFT)
[111-1<1>..............] (eraseone, 1) -> (subone, =, LEFT)
[111-<1>=..............] (subone, 1) -> (subone, 1, LEFT)
[111<->1=..............] (subone, -) -> (skip, -, LEFT)
[11<1>-1=..............] (skip, 1) -> (scanright, ., RIGHT)
[11.<->1=..............] (scanright, -) -> (scanright, -, RIGHT)
[11.-<1>=..............] (scanright, 1) -> (scanright, 1, RIGHT)
[11.-1<=>..............] (scanright, =) -> (eraseone, ., LEFT)
[11.-<1>...............] (eraseone, 1) -> (subone, =, LEFT)
[11.<->=...............] (subone, -) -> (skip, -, LEFT)
[11<.>-=...............] (skip, .) -> (skip, ., LEFT)
[1<1>.-=...............] (skip, 1) -> (scanright, ., RIGHT)
[1.<.>-=...............] (scanright, .) -> (scanright, ., RIGHT)
[1..<->=...............] (scanright, -) -> (scanright, -, RIGHT)
[1..-<=>...............] (scanright, =) -> (eraseone, ., LEFT)
[1..<->................] (eraseone, -) -> (HALT, ., LEFT)
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

If you find any bugs while using the program please submit an issue so that the bug can be fixed.

## License
[MIT](https://choosealicense.com/licenses/mit/)